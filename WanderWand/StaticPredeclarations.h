#include "stdafx.h"

#pragma once

Game game;

bool		Game::keystate[255];
vector2i	Game::mousePos;
float		Game::mouseSensitivity = 0.15f;
Player		Game::player;
Camera		Game::mainCamera;
bool		Game::captureMouse = true;
bool		Game::free3DMovement = false;
//void		(*Scene)(Player, Camera) = NULL;
void(*Game::Scene)	(Player, Camera) = NULL;