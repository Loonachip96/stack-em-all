#pragma once

class Game
{
public:
	static bool		keystate[];

	static vector2i	mousePos;
	static bool captureMouse; // czy przechwytywa� kursor myszy?
	static bool free3DMovement; // czy pozwoli� na ruch w 3D?

	static Player	player;
	static Camera	mainCamera;
	static float	mouseSensitivity;

	static void(*Scene)(Player, Camera);

	

public:
	Game();
	~Game();

	static void OnRender();
	static void OnReshape(int width, int height);
	static void OnKey(unsigned char key, int x, int y);
	static void OnKeyUp(unsigned char key, int x, int y);
	static void OnKeyDown(unsigned char, int, int);
	static void OnMouseMove(int x, int y);
	static void OnUpdate(int timerID);
};


