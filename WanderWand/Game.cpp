#include "stdafx.h"
#include "Game.h"


Game::Game()
{
	mouseSensitivity = 0.15f;
	for (int i = 0; i < 255; i++)
		Game::keystate[i] = false;
	Game::Scene = MainMenu;
}


Game::~Game()
{
}

void Game::OnRender()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(
		player.pos.x, player.pos.y, player.pos.z,
		player.pos.x + player.dir.x, player.pos.y + player.dir.y, player.pos.z + player.dir.z,
		0.0f, 1.0f, 0.0f
		);


#pragma region Swiatlo
	float l0_amb[] = { 0.2f, 0.2f, 0.2f };
	float l0_dif[] = { 0.6f, 0.6f, 0.6f };
	float l0_spe[] = { 1.0f, 1.0f, 1.0f };
	float l0_pos[] = { 1.0f, 5.0f, 4.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_AMBIENT, l0_amb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, l0_dif);
	glLightfv(GL_LIGHT0, GL_SPECULAR, l0_spe);
	glLightfv(GL_LIGHT0, GL_POSITION, l0_pos);
#pragma endregion

	Scene(player, mainCamera);

	glutSwapBuffers();
	glFlush();
	glutPostRedisplay();
}

void Game::OnUpdate(int timerID)
{
	glutTimerFunc(69, OnUpdate, 0);

#pragma region Ruch kamery

	if (captureMouse) {
		mainCamera.velRY = -mouseSensitivity * (glutGet(GLUT_WINDOW_WIDTH) / 2 - mousePos.x);
		mainCamera.velRX = mouseSensitivity * (glutGet(GLUT_WINDOW_HEIGHT) / 2 - mousePos.y);
		glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);
	}

	if (keystate['w']) {
		mainCamera.velM = player.speed;
	}
	if (keystate['s']) {
		mainCamera.velM = -player.speed;
	}
	if (keystate['a']) {
		mainCamera.velS = -player.speed;
	}
	if (keystate['d']) {
		mainCamera.velS = player.speed;
	}
	if (keystate['q']) {
		mainCamera.velRY = -player.speed;
	}
	if (keystate['e']) {
		mainCamera.velRY = player.speed;
	}
	if (keystate['f']) {
		mainCamera.velRX = -player.speed;
	}
	if (keystate['c']) {
		mainCamera.velRX = player.speed;
	}

	// Obr�t kamery (wsp. sferyczne):
	float T = acos(player.dir.y);
	float G = atan2(player.dir.z, player.dir.x);
	T -= mainCamera.velRX * .03f;
	G += mainCamera.velRY * .03f;
	player.dir.x = sin(T) * cos(G);
	player.dir.y = cos(T);
	player.dir.z = sin(T) * sin(G);

	// Wektor prostopad�y:
	vector3f per;
	per.x = -player.dir.z;
	per.y = 0;
	per.z = player.dir.x;

	// Ruch przod/tyl:
	player.pos.x += player.dir.x * mainCamera.velM * .1f;
	if (free3DMovement) {
		player.pos.y += player.dir.y * mainCamera.velM * .1f;
	}
	else {
		player.pos.y = 1.0f;
	}
	player.pos.z += player.dir.z * mainCamera.velM * .1f;

	// Ruch na boki:
	player.pos.x += per.x * mainCamera.velS * .1f;
	if (free3DMovement) {
		player.pos.y += player.dir.y * mainCamera.velM * .1f;
	}
	else {
		player.pos.y = 1.0f;
	}
	player.pos.z += per.z * mainCamera.velS * .1f;

	// Inercja:
	mainCamera.velRX /= 1.2;
	mainCamera.velRY /= 1.2;
	mainCamera.velM /= 1.2;
	mainCamera.velS /= 1.2;

#pragma endregion
}

void Game::OnReshape(int width, int height)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, width, height);
	gluPerspective(60.0f, (float)width / height, .01f, 100.0f);
}

void Game::OnKey(unsigned char key, int x, int y)
{
	if (!Game::keystate[key]) {
		Game::keystate[key] = true;
		OnKeyDown(key, x, y);
	}
}

void Game::OnKeyUp(unsigned char key, int x, int y)
{
	Game::keystate[key] = false;
}

void Game::OnKeyDown(unsigned char key, int x, int y)
{
	if (key == 27) {
		glutLeaveMainLoop();
	}
}

void Game::OnMouseMove(int x, int y)
{
	Game::mousePos = { x, y };
}

