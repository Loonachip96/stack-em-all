#include "stdafx.h"

#include "StaticPredeclarations.h"

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);

	glutInitWindowPosition(100, 100);
	glutInitWindowSize(640, 360);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

	glutCreateWindow("WanderWand");
	
	
	glutDisplayFunc( Game::OnRender );
	glutReshapeFunc( Game::OnReshape );
	glutKeyboardFunc( Game::OnKey );
	glutKeyboardUpFunc( Game::OnKeyUp );
	glutMotionFunc( Game::OnMouseMove );
	glutTimerFunc(69, Game::OnUpdate, 0);
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	glEnable(GL_LIGHTING);
	glShadeModel(GL_SMOOTH);

	float gl_amb[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, gl_amb);

	glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH) / 2, glutGet(GLUT_WINDOW_HEIGHT) / 2);
	game.mousePos.x = glutGet(GLUT_WINDOW_WIDTH) / 2;
	game.mousePos.y = glutGet(GLUT_WINDOW_HEIGHT) / 2;
	glutSetCursor(GLUT_CURSOR_NONE);
	
	glutMainLoop();

    return 0;
}

