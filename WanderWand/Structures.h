#pragma once

#ifndef STRUCTS
#define STRUCTS

struct vector2i {
	int x;
	int y;
};

struct vector3f {
	float x;
	float y;
	float z;
};

#endif // !STRUCTS

