// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <math.h>

#include <GL/freeglut.h>

#include "Bitmap.h"
#include "GlutSupportFxs.h"

#include "Structures.h"
#include "Camera.h"
#include "Player.h"
#include "Levels.h"

#include "Game.h"


