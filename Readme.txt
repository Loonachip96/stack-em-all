 /^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^\
/                                         \
\             Stack 'em all!              /
/                 Manual                  \
\                                         /
 \^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^v^/
 
 
 
######################################
#                                    #
#             Controls:              #
#                                    #
######################################

	Layer swap:			Q, E
	Move block toward direction:	W, S, A, D
	
	Change background: 	B
	Change block's texture:	N
	
	Restart: 	R
	Exit:		Esc
	
	
	
######################################
#                                    #
#             Game rules:            #
#                                    #
######################################	

	Every level have at least one layer
	
	Player shows the direction the blocks moves. Once done that, all the blocks along all the layers moves toward it as far as they can, without hitting an obstacle.

	Blocks that collapses, becomes one.
	
	Player wins, when blocks on each layer got stacked to one, before end of moves.
	
	
	
######################################
#                                    #
#       Create your own levels!      #
#                                    #
######################################
	
	Just simply add a notepad file named "NUMBER.lv", where NUMBER gets replaced by level number. They all must be named incrementing previous level name by 1, or they wouldn't be found.
	
	First row of .lv file contains in order:
		+ level width,
		+ level height,
		+ amount of layers,
		+ amount of moves
	separated with spaces.
	
	Further rows represents level shape. You can create a layer with numbers 0, 1 and 2, where each represents:
		+ 0 - empty grid
		+ 1 - a block
		+ 2 - a wall block

	Each created layer has to be separated by emply line.
	
	Here you have an example .lv file content:
	
	5 5 3 8
	2 2 2 2 2
	2 1 2 1 2
	2 0 1 0 2
	2 1 0 1 2
	2 2 2 2 2
	
	2 2 2 2 2
	2 1 2 1 2
	2 0 1 0 2
	2 2 0 1 2
	2 2 2 2 2
	
	2 2 2 2 2
	2 1 0 1 2
	2 0 1 2 2
	2 2 0 1 2
	2 2 2 2 2
	
	
	
######################################
#                                    #
#       Bring your own textures!     #
#                                    #
######################################
	
	To add new background, just place it in textures/bg folder and name it "NUMBER.bmp" (where NUMBER has to be created using rules from chapter above)
	
	To add blocks' texture collection, place them in textures/cubes folder, named "NUMBERf.bmp" for floating block, and "NUMBERs.bmp" for still blocks. The NUMBER in this case have to be additionally THE SAME for one textures' collection.
